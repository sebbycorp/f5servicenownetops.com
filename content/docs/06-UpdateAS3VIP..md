---
weight: 50
title: "Update AS3 VIP"
description: ""
icon: "article"
date: "2023-12-29T20:19:37Z"
lastmod: "2023-12-29T20:19:37Z"
draft: false
toc: true
---


## Let's update an AS3 Build
There are many cases where we need to update a cert, pool members ..etc so lets update our as3 we built 

1. Jump into the 'F5 Build Virtual Server' Catalog Item
2. Under Action select 'Update'
3. Under 'What is the name of the application?' type the name of the app 'demoas3'
4. Under 'Update the AS3 tenant' type the name of the tenant  'tenant_demoas3'
5. select the seach box 

Watch the f5 MID server API make the calls 


...