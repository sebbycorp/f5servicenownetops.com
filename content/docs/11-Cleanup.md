---
weight: 100
title: "Clean up"
description: ""
icon: "article"
date: "2023-12-29T20:19:37Z"
lastmod: "2023-12-29T20:19:37Z"
draft: false
toc: true
---


# Clean up

Let's clean up the environment. Execute the following code

```bash
terraform destroy
```

...