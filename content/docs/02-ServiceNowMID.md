---
weight: 10
title: "Mid Server Setup"
description: ""
icon: "article"
date: "2023-12-22T14:06:28-05:00"
lastmod: "2023-12-22T14:06:28-05:00"
draft: false
toc: true
---

## Setup the service Now mid server
To be able to access the F5 inside your datacentre or the cloud or any environment you need to deploy a MID server. A ServiceNow MID Server (Management, Instrumentation, and Discovery Server) acts as an intermediary that facilitates communication and data exchange between the ServiceNow platform and external systems, networks, and applications that reside within your local network or in private clouds. The MID Server enables ServiceNow to securely access and perform operations on business systems, even if those systems are behind firewalls or in secured environments.

## Configure MID Server Profiles
Now we need to configure our mid server profiles for this lab. In production, you might want to leave these settings. Or consult your service now team.

1. Go to your MID Server Settings within service now
![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn5.png?ref_type=heads)

2. Validate your MIDServer [ Not it might take 5 minutes to update it self]
![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn6.png?ref_type=heads)

3. Select your midserver and validate it.
![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn7.png?ref_type=heads)

![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn8.png?ref_type=heads)

4. Once its validate it should look like this
![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn9.png?ref_type=heads)

## Configure your MID Security Policy
Now we need to update the MID Security Policy to allow our MID servers to make communications to specific ips and remove some checks.
![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn10.png?ref_type=heads)

1. Select Intranet and uncheck Certifcate Chain Check and Hostname Check
![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn11.png?ref_type=heads)
![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn12.png?ref_type=heads)


...