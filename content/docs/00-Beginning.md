---
weight: 1
title: "Beginning"
description: ""
icon: "article"
date: "2023-12-22T14:06:28-05:00"
lastmod: "2023-13-22T14:06:28-05:00"
draft: false
toc: false
---

## What are we going to build

This Self Service Guide/Lab introduces you to build Service Now Catalogs to orchestrate your F5 BIGIP Infrastructure. The diagram below outlines what is going to be build.

![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/F5SNOWOverall.png?ref_type=heads)

* Deploy F5 in AWS with some applications
* Deploy MID Server in AWS
* Initiate a Development service now portal
* Enable the service now features for flow designer

## Start with Service Now

Service now allows you to spin up a developer environment that you can play with, unless you already have a dev environment in your work/lab.

* Register (Free) at https://developer.servicenow.com
* Start a Tokyo Instance [ When you start an instance you will get a url for servicenow and credentials], it will look like this. 

### Step 1 Activate Pluggins
Once you hace access to service now and have started up your service, you will need to activate the appropriate plugins.

![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn1.png?ref_type=heads)

* Activate the ServicewNow Integration Hub Enterprise package 

![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn2.png?ref_type=heads)


### Validate Service Now
Log into you server now account and lets validate your settings.

* Log into service now and make sure you have flow designer up and running

![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn3.png?ref_type=heads)

### Update my Service Now Demo Update Set

* Download the following file https://raw.githubusercontent.com/maniak-academy/f5servicenow-aws-demo/main/service%20now%20f5%20update%20set.xml
* wget https://raw.githubusercontent.com/maniak-academy/f5servicenow-aws-demo/main/service%20now%20f5%20update%20set.xml

#### Step 1 Import 
![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn4.png?ref_type=heads)

1. Go to Retrieved Update Sets
2. Click on 'Import Update Set from XML'
3. Find the file and import it
4. Commit the update set: Open the update set you just uploaded, from the list of retrieved update sets.
5. You won't be able to sort by the "loaded" date necessarily, so you may need to search by name.
6. If the update set has not been previewed already (if you don’t see the Commit Update Set button), click the Preview button.
7. If there are any preview errors, you’ll have to tell the system how to handle them using the Preview errors related list. 
8. Click Commit Update Set.

That's all there is to it!

...
