---
weight: 20
title: "Service Now Credentials"
description: ""
icon: "article"
date: "2023-12-29T20:19:37Z"
lastmod: "2023-12-29T20:19:37Z"
draft: false
toc: true
---

## Let's Update our credntials 
Since we uploaded a dummy environment we will need to update the credntials within Service Now. 

1. Lets update our F5 AWS Credentials, jump into "Credentials"
![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn13.png?ref_type=heads)

2. select F5AWS and update the password and click UPDATE
![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn14.png?ref_type=heads)

## Update Catalogue with Your F5 IPs
Now lets update the Service Now Catalog

1. Using the search "All" on the left side, type in 'Service Catalog'
2. Using the Search on the left side, type f5 and press enter
3. Select the 'F5 Build Virtual Server'
4. Now lets dive in and change some parameters, so put your mouse pointer to the top and right click and go to 'Configure Item'
![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn15.png?ref_type=heads)
5. Scroll down and find the 'Variables' section a 'Env' variable
![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn16.png?ref_type=heads)
6. Change the AWS environment with your BIGIP IP ADDRESS
![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/sn17.png?ref_type=heads)
7. Click Update



...