---
weight: 2
title: "Deploy Env"
description: ""
icon: "article"
date: "2023-12-22T14:06:28-05:00"
lastmod: "2023-13-22T14:06:28-05:00"
draft: false
toc: false
---

## Deploy F5 AWS Environment [Optional]
This is optional, you can use your own F5 environment or use the following provided. 

![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/F5SNOWOverall.png?ref_type=heads)

I built a terraform code that will deploy you a simple F5 environment in the cloud with the following tools

* F5 BIGIP 
* Service Now MID Server (This is used to connect to insdie your cluster, or onprem enviromnments)
* 2 Applications that hosts httpbin webapp


## Terraform code
Note you will need Terraform installed on your computer. Follow the following steps.

1. Clone the repo

```bash
git clone https://github.com/maniak-academy/f5servicenow-aws-demo.git
```

2. Edit the following files, midserver.sh and change the following parameters to match your service now developement env and your password.

* SN_HOST_NAME=<urname>.service-now.com 
* PASSWORD='password'

```bash
sudo docker run -d --name docker-2024 --env SN_HOST_NAME=<urname>.service-now.com/ --env USER_NAME=admin --env PASSWORD='password' moers/mid-server:tokyo.latest
```

3. Jump into the directory.
4. Configure aws credentials 

```
aws configure
```
5. You will need to subscribe within your AWS environment to deploy the F5. Click this link https://aws.amazon.com/marketplace/pp/prodview-nlakutvltzij4
6. execute terraform init, plan and apply

```bash
terraform init
terraform plan
```

7. If the terraform plan is successful, apply the terraform state. This will take 8 minutes to deploy an F5 in AWS. 

```bash
terraform apply
```

8. When the configuration is complete you will see the output that will provide you the following information
* F5 MGMT IP
* F5 username/password
* HTTPBIN servers  

9. Validate you can log into the F5 device https:<ip:>8443


...