---
weight: 30
title: "Build a Core F5 VIP"
description: ""
icon: "article"
date: "2023-12-29T20:19:37Z"
lastmod: "2023-12-29T20:19:37Z"
draft: false
toc: true
---


## Let's Build a Core VIP
So now that service now is setup and all the IPs and credentials are in place, let's build a vip. Here is an image of what we will build

![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/f51.png?ref_type=heads)

1. Log into our service now web port https://dev202618.service-now.com/sp 
2. Find out 'F5 Build Virtual Server' 


## Build a Core App
Lets build a HTTPS Core/Classic web application with the following parameters (not your external or node ips might be different )

![alternative text](https://gitlab.com/sebbycorp/f5servicenow.gitnetops.com/-/raw/main/content/docs/f52.png?ref_type=heads)

* VIP IP 10.0.1.80 443
* pool members ["10.0.1.151","10.0.1.202"] service port 80
* Key Examples

```bash
-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDH8w19FnKIg6KkW1mwz3wYb9mOWjVzg+NkzyGi2bAjw+keLzc6rAm4g3gV3GMigRc7tOnQMDF7mKhWLYWEELQY59QXSzhkJNN3WWef+ZY7MnpZEn/BRo6mnwaAZ/kPyXE0ge8jUXy/VkyI9jyDrBej9w9J/6busnfJ+g/db0K+RMhPkf8kO7NYO0mEU8dlTkSta1undb3L/Bs1nT8XcDNSdO3980ZTAhrkfOyFwduUvPdBaxmZS6ZNS1EX3Z6dfVKD9QaM/rPkcZzxA/Cz/ra2q6r2IaT8cj3fUr4S1W5rQEXk1NB8SGhe3ByRDTlip+5CXyXYx9OG+34AMB0+piRnAgMBAAECggEAFIjLEVYdVBTEvsFc8DIWygGuo5ZzF9ZrI368MxfNJZcomT97GgzyAJ+L1R7plXERfEHDo2xXoJxYaWVKaXo3rLkeSHgipfI8P20QndbJjXHNK9mhAi3nV4hMaCKFaxAijqRrdUKYn6PSv1wecd1aHi4wxUY6lBgDAlyqhXC4N4b1HtdwKM8Gb7IOMgj4wZJc9hunShiV4pqLJMKLz6EhZxPCpqh1JYN6PyDeBmatjOu0t9rdzbdggnzePkAzCN0kgrGFvhbhCgLONjE5yUeMigRdAtKgsKr6ADiFaBtPLNyxaAV6hQGFzMsDdTVMyEwQk7m0023u9bP6pD2b/xNOUQKBgQD6lDDy9ScoS/74VZB7RRgemN4QHMSMZtvy8eIGAmkejWghfTwwwkT7PGFU5YRsKv6RiWxFBbb+7ytEhlIgt7T5nblQpoEsd5uTEXVDzYHjSo4i7WMTvn2pgtn8SF1DxlgDPCiD2Se4jxZ73uTs9t1jLqE+GIjH56GTNu0y2nrFMQKBgQDMRnRtQst3kKhXzCYQtlWVhrsmsdEWMTWefMJBxELG8Mj0C0tkrlNuZ+FHDk7RxRiTMC90ABvmiAHb4NBOhYa4pEckitHF/4vNn+gi7N1n2sPmIK7DUu0BBaKXbuPV0VtsL5JIBDZkwUS2/tjI9zK/RS12XG4x61lfHD74SU39FwKBgQDStSxYEQflXcpAforo7Vfz2q3YtKgePBw2NWnPjdVhKJ1ok8u6YJHrBYH/BqUM5DZ+oSVXARA5XVpv29dRxci7ogKDrA+xGb7Ls4F9C2FuhHHzMtT+dE9s7ChreVpbtHyr87lJ7Z/2FPReIzczf/73+O0Dr1PNbZyzkxtwiMF18QKBgQCxvmhWHfWvHubsJk3E4vLLPcWg4L5/Ieh29DWPRjeYpTEl8KH0hB9ChQ8nZdK9glNk11ujAulECXocmZyHhnX4ObFRoZzwPPCnxgkWQqZQVLF6j1uhx4lEB2Q3ghyrExUKqMYO9eNifizNUijNjRLSDV6Gi9yygQdt08UTA63hlwKBgQCehWXUnZh9bN4eU1Bff3KXShrfytoxEhVH6eiV7Edt6/E1MWOoSwn3TjU91znPtNFZ6/KUAL/WOLgW/3IYRs4/Nxad3nQEqjjhKs0KdLzxTM+I6jTP0cnT9z75EgwwD51v5S1miHIPVhXWipngP6oPwgw6rDwDOT+vRFMyibaKJQ==\n-----END PRIVATE KEY-----
```

* Cert example

```bash
-----BEGIN CERTIFICATE-----\nMIIDUjCCAjqgAwIBAgIEGXxE4jANBgkqhkiG9w0BAQsFADBrMQswCQYDVQQGEwJDQTEMMAoGA1UECBMDb250MRAwDgYDVQQHEwdUb3JvbnRvMRIwEAYDVQQKEwlNeUNvbXBhbnkxDjAMBgNVBAsTBU15T3JnMRgwFgYDVQQDEw9kZW1vLm1hbmlhay5sYWIwHhcNMjMwNzIxMDI0MTM4WhcNMzMwNzE4MDI0MTM4WjBrMQswCQYDVQQGEwJDQTEMMAoGA1UECBMDb250MRAwDgYDVQQHEwdUb3JvbnRvMRIwEAYDVQQKEwlNeUNvbXBhbnkxDjAMBgNVBAsTBU15T3JnMRgwFgYDVQQDEw9kZW1vLm1hbmlhay5sYWIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDH8w19FnKIg6KkW1mwz3wYb9mOWjVzg+NkzyGi2bAjw+keLzc6rAm4g3gV3GMigRc7tOnQMDF7mKhWLYWEELQY59QXSzhkJNN3WWef+ZY7MnpZEn/BRo6mnwaAZ/kPyXE0ge8jUXy/VkyI9jyDrBej9w9J/6busnfJ+g/db0K+RMhPkf8kO7NYO0mEU8dlTkSta1undb3L/Bs1nT8XcDNSdO3980ZTAhrkfOyFwduUvPdBaxmZS6ZNS1EX3Z6dfVKD9QaM/rPkcZzxA/Cz/ra2q6r2IaT8cj3fUr4S1W5rQEXk1NB8SGhe3ByRDTlip+5CXyXYx9OG+34AMB0+piRnAgMBAAEwDQYJKoZIhvcNAQELBQADggEBABkaaW2F8kS4KoNGWoPv10U44Fkv9eFkR+sm5FeykUyhRR9g4CZVS+8RtTliYnqiLzy8NJVhZdgtkI7XlNyNjAD/gCu326zsmXJE3+R7XeyaPV9XEBrFZWVcMS7Ol0kJjGUIIYbBKdOFBC/XkHC+H5hPmXNRMVg7KKq5hj6h47xLg7Ugll4DwbYNECiIOK6+771vwhVkdseCeFbB/RTSLGbhxvD6jwY/pfQ1TJq3OOQIR9iAeW3qvyTfaiyOa0dwHrvKnHMegid05i3Hgf9Bp/tn8tUNNKhCOJ+eC4SRUNbgzjEg2fa0qnaK6M3F+GTrxYNCoCP0aRm/bBX0corUXOI=\n-----END CERTIFICATE-----
```

## Order/Approve and check if it worked.